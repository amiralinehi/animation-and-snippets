/* eslint-disable react/no-unescaped-entities */
import { IoIosArrowDown } from "react-icons/io";
import ListItem from "./ListItem";

// eslint-disable-next-line react/prop-types
const Navlinks = () => {
  return (
    <>
      <div className="relative group hidden md:block">
        <div className="flex justify-center items-center gap-1">
          <a className="font-semibold text-[1rem]">MEN</a>
          <IoIosArrowDown
            size={24}
            className="group-hover:rotate-180"
          ></IoIosArrowDown>
        </div>
        <div className="group-hover:block hidden bg-transparent absolute w-[32rem] h-[32rem]">
          <div className="bg-transparent relative w-full h-full">
            <div className="bg-white w-full h-full absolute grid grid-cols-3 gap-6 p-4 mt-[4rem]">
              <div className="bg-white mt-4 w-[1.5rem] h-[1.5rem] rotate-45 absolute -top-5 left-5"></div>
              <ListItem typewear={"TOPWEAR"}></ListItem>
              <ListItem typewear={"BOTTOMWEAR"}></ListItem>
              <ListItem typewear={"INNERWEAR"}></ListItem>
              <ListItem typewear={"SLEEPWEAR"}></ListItem>
              <ListItem typewear={"FOOTWEAR"}></ListItem>
            </div>
          </div>
        </div>
      </div>

      <div className="relative group hidden md:block">
        <div className="flex justify-center items-center gap-1">
          <a className="font-semibold text-[1rem]">WOMEN</a>
          <IoIosArrowDown
            size={24}
            className="group-hover:rotate-180"
          ></IoIosArrowDown>
        </div>
        <div className="group-hover:block hidden bg-transparent absolute w-[32rem] h-[32rem]">
          <div className="bg-transparent relative w-full h-full">
            <div className="bg-white w-full h-full absolute grid grid-cols-3 gap-6 p-4 mt-[4rem]">
              <div className="bg-white mt-4 w-[1.5rem] h-[1.5rem] rotate-45 absolute -top-5 left-5"></div>
              <ListItem typewear={"TOPWEAR"}></ListItem>
              <ListItem typewear={"BOTTOMWEAR"}></ListItem>
              <ListItem typewear={"INNERWEAR"}></ListItem>
              <ListItem typewear={"SLEEPWEAR"}></ListItem>
              <ListItem typewear={"FOOTWEAR"}></ListItem>
            </div>
          </div>
        </div>
      </div>

      <div className="relative group hidden md:block">
        <div className="flex justify-center items-center gap-1">
          <a className="font-semibold text-[1rem]">KID'S</a>
          <IoIosArrowDown
            size={24}
            className="group-hover:rotate-180"
          ></IoIosArrowDown>
        </div>
        <div className="group-hover:block hidden bg-transparent absolute w-[32rem] h-[32rem]">
          <div className="bg-transparent relative w-full h-full">
            <div className="bg-white w-full h-full absolute grid grid-cols-3 gap-6 p-4 mt-[4rem]">
              <div className="bg-white mt-4 w-[1.5rem] h-[1.5rem] rotate-45 absolute -top-5 left-5"></div>
              <ListItem typewear={"TOPWEAR"}></ListItem>
              <ListItem typewear={"BOTTOMWEAR"}></ListItem>
              <ListItem typewear={"INNERWEAR"}></ListItem>
              <ListItem typewear={"SLEEPWEAR"}></ListItem>
              <ListItem typewear={"FOOTWEAR"}></ListItem>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Navlinks;
