const ListItem = (props) => {
  return (
    <>
      <li className="flex flex-col gap-1 font-bold w-full text-[1rem]">
        {props.typewear}
        <div className="text-gray-600 text-[.85rem] font-semibold my-1.5 hover:text-[#4A3AFF] cursor-pointer">
          T-SHIRTS
        </div>
        <div className="text-gray-600 text-[.85rem] font-semibold my-1.5 hover:text-[#4A3AFF] cursor-pointer">
          CASUAL SHIRTS
        </div>
        <div className="text-gray-600 text-[.85rem] font-semibold my-1.5 hover:text-[#4A3AFF] cursor-pointer">
          FORMAL SHIRTS
        </div>
        <div className="text-gray-600 text-[.85rem] font-semibold my-1.5 hover:text-[#4A3AFF] cursor-pointer">
          FORMAL SHIRTS
        </div>
        <div className="text-gray-600 text-[.85rem] font-semibold my-1.5 hover:text-[#4A3AFF] cursor-pointer">
          FORMAL SHIRTS
        </div>
      </li>
    </>
  );
};

export default ListItem;
