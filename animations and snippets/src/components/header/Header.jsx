import { useState } from "react";
import Navitems from "./NavItems";

const Header = () => {
  const [menu, setMenu] = useState(false);

  const toggleMenu = () => {
    setMenu(!menu);
  };

  return (
    <section className="bg-cover bg-top h-screen bg-Header flex justify-center items-center flex-col gap-0 relative transition-all">
      <Navitems onClick={toggleMenu} status={menu}></Navitems>
      <div className="text-white text-[1.75rem] font-semibold">
        Fashion Tips
      </div>

      <div className="text-white text-[2rem] md:text-[3.25rem] font-bold pb-5 mb-0 text-center">
        Items every woman should have
      </div>
      <button className="bg-[#4A3AFF] font-semibold text-[1.25rem] text-white py-[.5rem] px-[1.75rem] rounded-full text-center">
        Get Started
      </button>

      <div
        className={`md:hidden transition-all delay-100 flex flex-col justify-center items-center w-full h-full bg-yellow-400 absolute -left-[100%] ${
          menu ? "-left-[0%]" : ""
        }`}
      >
        hello
      </div>
    </section>
  );
};

export default Header;
