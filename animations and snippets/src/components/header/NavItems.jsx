import Logo from "../../assets/logo.png";
import Navlinks from "./Navlinks";
import { IoMenu } from "react-icons/io5";
import { IoCloseOutline } from "react-icons/io5";

const Navitems = ({ onClick, status }) => {
  return (
    <div className="bg-white w-full absolute top-0 flex flex-row justify-around items-center gap-16 py-5 px-0 z-10">
      <div className="h-12 w-fit md:m-auto">
        <img src={Logo} alt="logo-img" className="h-[2rem] w-full block pl-2" />
      </div>

      <div className="font-semibold text-[1rem] hidden md:block">HOME</div>

      <div className="flex justify-center items-center w-fit gap-12">
        <Navlinks></Navlinks>
      </div>

      <div>
        {!status ? (
          <IoMenu onClick={onClick} className="md:hidden" size={36}></IoMenu>
        ) : (
          <IoCloseOutline
            onClick={onClick}
            className="md:hidden"
            size={36}
          ></IoCloseOutline>
        )}
      </div>

      <button className="hidden md:inline-block bg-[#4A3AFF] text-[1rem] text-white py-2 px-8 rounded-full h-fit m-auto whitespace-nowrap">
        Get started
      </button>
    </div>
  );
};

export default Navitems;
